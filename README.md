# Python_Tutorial

## Name
Python tutorial for me.

## Description
I created this project to monitor the progress of my python training, and for version control.

## Usage
```
jupyter notebook <jupyter notebook>
```
## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:7e17b1be8b26e4268764355841d64b6d?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:7e17b1be8b26e4268764355841d64b6d?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:7e17b1be8b26e4268764355841d64b6d?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/Ayorinde_Afo/python_tutorial.git
git branch -M main
git push -uf origin main
```

## Authors and acknowledgment
Myself


